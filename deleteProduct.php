<?php
  session_start();
  
  require_once("model/database/schemas/postgresql/postgresql.php");

  require_once("model/product.php");
  
  $returnMessage = "";
  
  $code = $_GET['sku'];

  $modelProduct = new Product();

  $product = $modelProduct->readProductCode($code);

  if(isset($_POST['deleteProduct'])){
    $code = $_POST['codeProductDelete'];

    $response = $modelProduct->delProduct($code);
    
    if($response['status'] === "200"){
        Header("Location:http://192.168.15.24/products.php?status=success");
    }else{
        Header("Location:http://192.168.15.24/products.php?status=danger");
    }
  }
  
  $productHTML = "";
  
  foreach($product as $value){
    $productHTML .= "
        <div class='sup'>
          <label>".$value['name']."</label> 
            <b style='display:flex;flex-direction:column;align-items:center;justify-content:center;'>:</b>
            <input data-js='".$value['name']."' 
                   class='in-cmp' 
                   type='text' 
                   id='form-".$value['name']."' 
                   onblur='this.placeholder = '".$value['name']."'' 
                   placeholder='".$value['name']."'
                   value='".$value['name']."'                    
                   autocomplete='off'
                   name='codeCategoryDelete'
                   maxlength='100'/>
            <input type='hidden' value='".$value['sku']."' name='codeProductDelete' />'
        </div>
      ";  

  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="./css/estilos.css" />
    <title>Webjump | Backend Test | Categories</title>
</head>
<body>
    <header>
        <div class="header-barber">
            <b>WEB JUMP</b>
        </div>
    </header>
    <div class="back-to-index">
      <a href="http://192.168.15.24/products.php">	&larr;</a>
    </div>
    <div class="component-header">
            <div class="component-info">
                <div class="info">
                    <div class="title-website">
                        <h1>Delete Categories : </h1>
                    </div>
                </div>
            </div>
        </div>
    <main>
            <div class="grid-menu">
                <div class="component-form">
                <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                        <?php echo $productHTML; ?>
                        <button type="submit" class="form-submit" id="buttonForm btn-danger" name="deleteProduct">Delete Products</button>
                    </form>
                </div>
            </div>
        </main>
        <script src="./js/main.js"></script>
</body>
</html>
