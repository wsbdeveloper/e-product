<?php
  session_start();  

  require_once('PHPMailer/src/mail.php');

  require_once('model/database/schemas/postgresql/postgresql.php');

  $sendEmail = new Email();

  if(isset($_POST['formSendEmail'])){
    try{
      $db = new Database();
      $connect = $db->connect();
      
      $table = "product";
      $file = $_POST['fileName'].'.csv';
      $delimiter = ",";
      $as_null = "\\\\N";
      
      $connect->pgsqlCopyToFile((string)$table,(string)$file,(string)$delimiter,(string)$as_null,'name,sku');
      $sendEmail->sendReport($_POST['email'],$_POST['name'],$file);
    }catch(Exception $err){
      $err->getMessage();
    }

 
  }

  $status = "";

  if(isset($_GET['status'])){
    $status .= $_GET['status'];
  } 

  $returnMessage = "";
  
  if($status === 'success'){
    $returnMessage .= "
      <div class='alert alert-success' role='alert'>
        Arquivo de relatório criado com sucesso, verifique seu email!
      </div>
    ";
  }else if ($status === 'danger'){
    $returnMessage .= "
      <div class='alert alert-danger' role='alert'>
        Alteração não foi executada com sucesso!
      </div>
    ";
  }
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="./css/estilos.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Webjump | Backend Test | Categories</title>
</head>
<body>
    <header>
        <div class="header-barber">
            <b>WEB JUMP</b>
        </div>
    </header>
    <div class="back-to-index">
      <a href="http://192.168.15.24/index.php">	&larr;</a>
    </div>
    <div class="component-header">
            <div class="component-info">
                <div class="info">
                    <div class="title-website">
                        <h1>Send Report for Email</h1>
                    </div>
                </div>
            </div>
            <div class="form-return"></div>
            <?php echo $returnMessage; ?>
        </div>
      <main>
        <div class="row" style="justify-content: center;">
          <div class="col col-md-5">
            <form action='<?php echo $_SERVER['PHP_SELF'] ?>' method='post' onsubmit="validate(e)">
              <label for="name" class="label-for-email"><strong>Name</strong></label>
              <input class="form-control" id="name-form" name="name" type="text" placeholder="Wellington da Silva Bezerra" />
              <br>
              <label for="name" class="label-for-email"><strong>File Name</strong></label>
              <input class="form-control" id="report-form" name="fileName" type="text" placeholder="Report" />
              <br>
              <label for="email" class="label-for-email"><strong>Email</strong></label>
              <input class="form-control" id="email-form" name="email" type="email" placeholder="email@domain.com" />
              <button class="form-submit" id="btn-form" name="formSendEmail" style="margin:1.23em auto;display:block;" type="submit" > Send </button>
            </form>
          </div>
        </div>
      </main>
    
      <script src="./js/main.js"></script>
</body>
</html>
