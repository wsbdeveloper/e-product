<?php

  session_start();
  
  require_once("model/product.php");
 
  require_once("model/category.php");
  
  require_once("model/product_category.php");


  try{

    $categories = new Category();

    $getCategories = $categories->readCategory();

    $categoriesHTML = ""; 

    foreach($getCategories as $key => $value){
      $categoriesHTML .= "
        <div class='form-check'>
          <input class='form-check-input' type='checkbox' name='arrayCategories[]' value=".$value['code'].">
          <label class='form-check-label'>".$value['name']."</label>
        </div>";
    }


    if(isset($_POST["submitNewProduct"])) {
      $name = $_POST['name'];
      $sku  = $_POST['sku'];
      $price= $_POST['price'];
      $describe = $_POST['describe'];
      $quantity = $_POST['quantity'];
      $arrayCategories = $_POST['arrayCategories'];
      $category = $arrayCategories[0];
      $photo = $_POST['photo'];

      $formats = array("jpeg","png","jpg","gif");

      $ext = pathinfo($_FILES['photo']['name'],PATHINFO_EXTENSION);
    
      if(in_array($ext,$formats)){
        $path = "/var/www/html/uploads/";
        $tmp  = $_FILES['photo']['tmp_name'];
        $photo = "$sku".".$ext";
    
        if(move_uploaded_file($tmp, $path.$photo)){
          $product = new Product(); 
          $product->newProduct(
            (string)$name,
            (string)$sku,
            (double)$price,
            (string)$describe,
            (int)$quantity,
            (string)$category,
            (string)$photo
          );

          $pivoProductCategory = new productCategory();

          foreach($arrayCategories as $cat){
              $pivoProductCategory->insertNewData((string)$cat,(string)$sku);
          }
          Header("Location:http://192.168.15.24/index.php?status=success");
        }
      }else{
        return http_response_code(400);
      } 
    }
  }catch(Exception $e){
    $e->getMessage();
  }
?>
<!doctype html>
<html lang="en">
<head>
  <title>Webjump | Backend Test | Add Product</title>
  <meta charset="utf-8">
  <link  rel="stylesheet" type="text/css"  media="all" href="css/style.css" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <meta name="viewport" content="width=device-width,minimum-scale=1">
  
  <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
  <script async src="https://cdn.ampproject.org/v0.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
</head>
  <!-- Header -->
  <amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="index.php"><img src="images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="categories.php" class="link-menu">Categorias</a></li>
      <li><a href="products.php" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="index.php" class="link-logo"><img src="images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header -->
  <!-- Main Content -->
<main class="content">
    <h1 class="title new-item">New Product</h1>
    
    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" >
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" id="sku" class="input-text" name="sku" /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" class="input-text" name="name" /> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" min="1" step="any" id="price" class="input-text" name="price" data-js="money" /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="number" id="quantity" class="input-text" name="quantity"/> 
      </div>
      <div class="input-field container" style='width:50%'>
          <strong style="font-size:24px;">Categories</strong>
          <?php echo $categoriesHTML; ?>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" class="input-text" name="describe"></textarea>
      </div>
      <div class="input-field">
        <label for="file" class="label">Photo the product</label>
        <input class="" type="file" name="photo" />
      </div>
      <div class="actions-form">
        <a href="products.php" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" name="submitNewProduct" />
      </div>      
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="footer-image">
	  <img src="images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
	</div>
	<div class="email-content">
	  <span>go@jumpers.com.br</span>
	</div>
</footer>
 <!-- Footer --></body>
  <script src="./js/mask.js"></script>  
</html>
