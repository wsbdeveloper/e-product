<?php
  session_start();
  
  require_once("model/database/schemas/postgresql/postgresql.php");

  require_once("model/product.php");
  
  $sku = $_GET['sku'];
  
  $status = "";

  if(isset($_GET['status'])){
    $status .= $_GET['status'];
  } 

  $modelProduct = new Product();

  $product = $modelProduct->readProductCode($sku);
  
  $productHTML = "";
  $returnMessage = "";
  if($status === 'success'){
    $returnMessage .= "
      <div class='alert alert-success' role='alert'>
        Alteração executada com sucesso!
      </div>
    ";
  }else if ($status === 'danger'){
    $returnMessage .= "
      <div class='alert alert-danger' role='alert'>
        Alteração não foi executada com sucesso!
      </div>
    ";
  }
  
  if(isset($_POST['submitEditProduct'])){
    $name = $_POST['name'];                                                   
    $sku  = $_POST['sku'];                                                    
    $price= $_POST['price'];                                                  
    $describe = $_POST['describe'];                                           
    $quantity = $_POST['quantity'];                                           
    $category = $product[0]['code_category'];
    

    try{
      $updateProduct = $modelProduct->updateProduct(
        (string)$name,
        (string)$sku,
        (double)$price,
        (string)$describe,
        (int)$quantity,
        (string)$category,
        (string)$sku
      );

      Header('Location:http://192.168.15.24/products.php?status=success');  
    }catch(Exception $error){
      $error->getMessage();
    }
  }


  foreach($product as $value){
    $productHTML .= "
      <div class='col-md-12'>
        <div class='card col-md-7' style='width: 100%;margin: 1em auto;box-shadow: 4px 5px 8px #d5cdcdcc;'>
          <img src='uploads/".$value['cdnphoto']."' style='width: 50%;margin: 2em auto;'class='card-img-top col-md-5' alt='".$value['name']."'>
          <div class='card-body'>
            <h3 class='card-title'>".$value['name']."</h3>
            <p class='card-text'>".$value['describe']."</p>
          </div>
          <form action='".$_SERVER['PHP_SELF']."' method='POST'>
          <ul class='list-group list-group-flush'>
            <li class='list-group-item' style='display:flex;'>                  
              <strong style='width: 140px;'>Name</strong><b class='mr-4'>:</b>
              <input type='text' class='form-control' name='name' value='".$value['name']."' />
            </li>
            <li class='list-group-item' style='display:flex;'>
              <strong style='width: 140px;'>Price</strong><b class='mr-4'>:</b>
                <input type='text' class='form-control' name='price' value='".$value['price']."' />
            </li>
            <li class='list-group-item' style='display:flex;'>
              <strong style='width: 140px;'>Describe</strong><b class='mr-4'>:</b>
              <input type='text' class='form-control' name='describe' value='".$value['describe']."' />
            </li>
            <li class='list-group-item' style='display:flex;'>                  
              <strong style='width: 140px;'>Quantity</strong><b class='mr-4'>:</b>
              <input type='number' class='form-control' name='quantity' value='".$value['quantity']."' />
            </li>
            <li class='list-group-item' style='display:flex;'>                  
              <strong style='width: 140px;'>SKU</strong><b class='mr-4'>:</b>
              <input type='text' class='form-control' name='sku' value='".$value['sku']."' />
            </li>
          </ul>
          <div class='card-body' style='display: flex;justify-content: center;'>
              <button type='submit' class='btn btn-outline-primary' name='submitEditProduct'>Edition</button>
          </div>
        </div>
      </div>
    ";
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="./css/estilos.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Webjump | Backend Test | Categories</title>
</head>
<body>
    <header>
        <div class="header-barber">
            <b>WEB JUMP</b>
        </div>
    </header>
    <div class="back-to-index">
      <a href="http://192.168.15.24/index.php">	&larr;</a>
    </div>
    <div class="component-header">
            <div class="component-info">
                <div class="info">
                    <div class="title-website">
                        <h1>Edit Product </h1>
                    </div>
                </div>
            </div>
            <?php echo $returnMessage; ?>
        </div>
      <main>
        <div class="row">
          <?php echo $productHTML; ?>
        </div>
      </main>
    
        <script src="./js/main.js"></script>
</body>
</html>
