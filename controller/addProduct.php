<?php
  require_once("../model/product.php");


  try{
    $db = new Database();
    $connect = $db->connect();

    $name     = $_POST['name'];
    $sku      = $_POST['sku'];
    $price    = (float)$_POST['price'];
    $describe = $_POST['describe'];
    $quantity = $_POST['quantity'];
    $category = $_POST['category'];

    $product = new Product();   
  
    $product->newProduct($name,$sku,$price,$describe,$quantity,$category);
     
  }catch(Exception $e){
    $e->getMessage();
  }
?>
