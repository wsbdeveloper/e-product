<?php
  require_once("../model/database/schemas/postgresql/postgresql.php");

  function getCategories(){
    $db = new Database();
    $connect = $db->connect();

    $getCategories = "SELECT name , code FROM category;";

    $statement = $connect->prepare($getCategories);
    
    $statement->execute();

    $res = $statement->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($res);
    return json_encode($res);
  }

  
  getCategories();


?>
