<?php
  require_once("database/schemas/postgresql/postgresql.php");

  class Category extends Database {
    public function __construct(){
      parent::__construct();
    }   


    public function newCategory($name,$code){
      $db = new Database();
      $connect = $db->connect();

      try{
        $newCategory = "INSERT INTO category VALUES (:name,:code)";

        $statement = $connect->prepare($newCategory);
        $statement->bindParam(':name',$name,PDO::PARAM_STR);        
        $statement->bindParam(':code',$code,PDO::PARAM_STR);        
        $statement->execute();

        if($statement->rowCount() > 0){
          $response = array(
            "status"=>"200",
            "response" => "Categoria : $name - $code, adicionada com sucesso!"
          );

          Header("Location:http://192.168.15.24/categories.php?status=success"); 
          return json_encode($response);
        }else{
          $response = array(
            "status" => "403",
            "response" => "Categoria não foi adicionada com sucesso!"
          );

          Header("Location:http://192.168.15.24/categories.php?status=danger");
          return json_encode($response);
        }    
      }catch(PDOException $erro){
        $erro->getMessage();
      }
    }

    public function updateNameTheCategory($name,$code){
      $db = new Database();
      $connect = $db->connect();
    
      try{
        $updateName = "UPDATE category SET name = :name WHERE code = :code;";
        
        $statement = $connect->prepare($updateName);

        $statement->bindParam(':name',$name,PDO::PARAM_STR);
        $statement->bindParam(':code',$code,PDO::PARAM_STR);
        
        $res = $statement->execute();

        if($statement->rowCount() > 0){
          $response = array(
            "status" => "200",
            "response" => "Nome modificado com sucesso!"
          );

          return json_encode($response);
        }else{
          $response = array(
            "status" => "400",
            "response" => "Update não foi realizado com sucesso!"
          );

          return json_encode($response);
        }

        
      }catch(PDOException $error){
        $error->getMessage();
      }   
    }


    public function updateCodeTheCategory($name,$code){
      $db = new Database();
      $connect = $db->connect();
    
      try{
        $updateCode = "UPDATE category SET code = :code WHERE name = :name;";

        
        $statement = $connect->prepare($updateCode);
        
        $statement->bindParam(':name',$name,PDO::PARAM_STR);
        
        $statement->bindParam(':code',$code,PDO::PARAM_STR);
        
        $statement->execute();


        if($statement->rowCount() > 0){
          $response = array(
            "status" => "200",
            "response" => "Código modificado com sucesso!"
          );

          return json_encode($response);
        }else{
          $response = array(
            "status" => "400",
            "response" => "Update não foi realizado com sucesso!"
          );

          return json_encode($response);
        }

        
      }catch(PDOException $error){
        $error->getMessage();
      }   
    }

    
    public function delCategory($code){
      $db = new Database();
      $connect = $db->connect();

      var_dump($code);      
      
      try{
        $delete = "DELETE FROM category WHERE code = :code";
        
        $statement = $connect->prepare($delete);

        $statement->bindParam(":code",$code,PDO::PARAM_STR);
        
        $statement->execute();
        
        var_dump($statement);
        
        if($statement->rowCount() > 0){
          $response = array(
            "status" => "200",
            "response" => "Categoria deletada com sucesso!"
          );
      
          return $response;
        }else{
          $response = array(
            "status" => "400",
            "response" => "ERRO: Não foi possível deletar categoria : $code"
          );

          return $response;
        }
      }catch(PDOException $error){
        $error->getMessage();
      }  
    }


    public function readCategory(){
      $db = new Database();
      $connect = $db->connect();

      try{
        $find = "SELECT name,code FROM category";
      
        $statement = $connect->prepare($find);
      
        $statement->execute();
      
        $dados = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $dados;
      }catch(PDOException $error){
        $error->getMessage();
      }
      
    }

    public function readCategoryCode($code){
      $db = new Database();
      $connect = $db->connect();

      try{
        $find = "SELECT name,code FROM category WHERE code = :code";

        $statement = $connect->prepare($find);
        
        $statement->bindParam(":code",$code,PDO::PARAM_STR);

        $statement->execute();

        $response = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $response;
      }catch(PDOException $error){
        $error->getMessage();
      }

    }

  }

  #$categoria = new Category();
  
  #$categoria->newCategory("Notebook DELL 1440","154622523");
  #$categoria->readCategory();
  #$categoria->delCategory("roupas1241");
  #$categoria->updateNameTheCategory("Notebook Lenovo","5746233123");
  #$categoria->updateNameTheCategory("Notebook Lenovo","5746233123");
  #$categoria->updateCodeTheCategory("Notebook Lenovo","5746233123");
?>
