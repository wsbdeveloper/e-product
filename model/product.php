<?php
  require_once("model/database/schemas/postgresql/postgresql.php");


  class Product extends Database {
    private $name;
    private $sku;
    private $price;
    private $describe;
    private $quantity;
    private $code_category;
    private $cdnphoto;
    
    #Not getters and setters, for functions simple running. Not necessary setVariables.

    public function __construct(){
      parent::__construct();
    }   


    public function newProduct($name,$sku,$price,$describe,$quantity,$code_category,$photo){
      try{
        $db = new Database();
        $connect = $db->connect();

        $newCategory = "INSERT INTO product (name,sku,price,describe,quantity,code_category,cdnphoto)
                        VALUES (:name,:sku,:price,:describe,:quantity,:code_category,:photo)";

        $statement = $connect->prepare($newCategory);

        $statement->bindParam(':name',$name);        
        $statement->bindParam(':sku',$sku);        
        $statement->bindParam(':price',$price);        
        $statement->bindParam(':describe',$describe);        
        $statement->bindParam(':quantity',$quantity);        
        $statement->bindParam(':code_category',$code_category);
        $statement->bindParam(':photo',$photo);
        
        $statement->execute();

        if($statement->rowCount() > 0){
          $response = array(
            "status"=>"200",
            "response" => "Produto adicionado com sucesso!"
          );
          
          return $response;
        }else{
          $response = array(
            "status" => "403",
            "response" => "Produto não foi adicionada com sucesso!"
          );

          return $response;
        }    
      }catch(PDOException $erro){
        $erro->getMessage();
      }
    }

    public function updateProduct($name,$sku,$price,$describe,$quantity){
      $db = new Database();
      $connect = $db->connect();
    
      try{
        $updateCode = "UPDATE product SET name = :name, sku = :sku , price = :price , describe = :describe , quantity = :quantity WHERE sku = :code";
        
        $statement = $connect->prepare($updateCode);

        $statement->bindParam(':name',$name,PDO::PARAM_STR);
        
        $statement->bindParam(':sku',$sku,PDO::PARAM_STR);
        
        $statement->bindParam(':price',$price,PDO::PARAM_STR);
        
        $statement->bindParam(':describe',$describe,PDO::PARAM_STR);
        
        $statement->bindParam(':quantity',$quantity,PDO::PARAM_INT);
        
        $statement->bindParam(':code',$sku,PDO::PARAM_STR);
        
        $statement->execute();

        if($statement->rowCount() > 0){
          $response = array(
            "status" => "200",
            "response" => "Código modificado com sucesso!"
          );

          return $response;
        }else{
          $response = array(
            "status" => "400",
            "response" => "Update não foi realizado com sucesso!"
          );

          return $response;
        }

        
      }catch(PDOException $error){
        $error->getMessage();
      }   
    }

    public function delProduct($sku){
      $db = new Database();
      $connect = $db->connect();
      
      try{
        $delete = "DELETE FROM product WHERE sku = :sku;";
        
        $statement = $connect->prepare($delete);
        
        $statement->bindParam(":sku",$sku,PDO::PARAM_STR);
        
        $statement->execute();
        
        if($statement->rowCount() > 0){
          $response = array(
            "status" => "200",
            "response" => "Produto deletado com sucesso!"
          );
      
          return $response;
        }else{
          $response = array(
            "status" => "400",
            "response" => "ERRO: Não foi possível deletar o Produto : $code"
          );

          return $response;
        }
      }catch(PDOException $error){
        $error->getMessage();
      }  
    }


    public function readAllProduct(){
      $db = new Database();
      $connect = $db->connect();


      try{
        $find = "SELECT * FROM product;";
      
        $statement = $connect->prepare($find);
      
        $statement->execute();
      
        $dados = $statement->fetchAll(PDO::FETCH_ASSOC);


        return $dados;
      }catch(PDOException $error){
        $error->getMessage();
      }
    }

    public function readProductCode($sku){
      $db = new Database();
      $connect = $db->connect();
      

      try{
        $find = "SELECT name, sku ,price ,describe,quantity,code_category,cdnphoto FROM product WHERE sku = :sku_code";
        
        $statement = $connect->prepare($find);
        
        $statement->bindParam(":sku_code",$sku,PDO::PARAM_STR);
        
        $statement->execute();

        $response = $statement->fetchAll(PDO::FETCH_ASSOC);
        
        return $response;
      }catch(Exception $err){
        $err->getMessage();

      }
    }
  }
  #$product = new Product();
  
  #$product->newProduct("Televisão SANSUMG","lo3233if1",2342.32,"Televisão SANSUMG 43 LED",3,"574622523","photo.png");
  #$product->readProduct();
  #$product->updateProduct("lo3213if1","name","Televisão PHP Update");
  #$product->delProduct("lo3213if1");
?>
