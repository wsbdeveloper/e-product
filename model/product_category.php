<?php
  require_once("model/database/schemas/postgresql/postgresql.php");


  class productCategory extends Database {
    public function __construct(){
      parent::__construct();
    }
    
    public function insertNewData($code_category,$sku_product){
      $db = new Database();
      $connect  = $db->connect();

      try{
          $insert = "INSERT INTO p_product_category (sku , code) VALUES (:sku_product ,:code_category)";
          
          $statement = $connect->prepare($insert);
        
          $statement->bindParam(':sku_product',$sku_product,PDO::PARAM_STR);
        
          $statement->bindParam(':code_category',$code_category,PDO::PARAM_STR);
          
          print_r($statement);
          $statement->execute();

          if($statement->rowCount() > 0){
            return http_response_code(200);
          }else{
            return http_response_code(400);
          }
      }catch(Exception $err){
        $err->getMessage();
      }
    }
  }

?>

