<?php
  session_start();

  require_once("model/database/schemas/postgresql/postgresql.php");
  

  $returnMessage = "";
  $status = "";

  if(isset($_GET["status"])){
    $status .= $_GET["status"];
  }

  if($status === 'success'){
    $returnMessage .= "
      <div class='alert alert-success' role='alert'>
        Alteração executada com sucesso!
      </div>
    ";
    
  }else if($status === 'danger'){
    $returnMessage .= "
      <div class='alert alert-danger' role='alert'>
        Alteração não foi executada com sucesso!
      </div>
    ";
  }else if(!$status){
    $retunMessage = "";       
  }

  function getProducts(){
    $db = new Database();
    $connect = $db->connect();

    $getProducts = "SELECT name, sku , price , describe , quantity ,code_category FROM product;";
    
    $statement = $connect->prepare($getProducts);
    
    $statement->execute();
    
    $res = $statement->fetchAll(PDO::FETCH_ASSOC);
    
    return $res;
  }

  function getCategories($productSKU){
    $db = new Database();
    $connect = $db->connect();

    $getCategories = "SELECT C.name FROM category AS C INNER JOIN p_product_category AS PC ON C.code = PC.code INNER JOIN product AS PR ON PR.sku = PC.sku WHERE PC.sku = '".$productSKU."'";
    
    
    $statement = $connect->prepare($getCategories);

    $statement->execute();

    $response = $statement->fetchAll(PDO::FETCH_ASSOC);
    
    return $response;
  }


  $products = getProducts();
  $productsHTML = "";

  foreach($products as $key => $value){
    $categories = getCategories($value['sku']);
    $categoriesHTML = "";

    foreach($categories as $cat){
      $categoriesHTML .= $cat["name"]."<br>";
    }

    $productsHTML .= "
      <tr class='data-row'>
        <td class='data-grid-td'>
          <span class='data-grid-cell-content'>".$value['name']."</span>
        </td>
        <td class='data-grid-td'>
          <span class='data-grid-cell-content'>".$value['sku']."</span>
        </td>

        <td class='data-grid-td'>
          <span class='data-grid-cell-content'>".$value['price']."</span>
        </td>

        <td class='data-grid-td'>
          <span class='data-grid-cell-content'>".$value['quantity']."</span>
        </td>

        <td class='data-grid-td'>
          <span class='data-grid-cell-content'>".$categoriesHTML."</span>
        </td>

        <td class='data-grid-td'>
          <div class='row' style='text-align: center;display: flex;justify-content: space-evenly;'>
            <div class='action edit'>
              <a href='http://192.168.15.24/editProduct.php?sku=".(string)$value['sku']."'>
                <button type='button' class='mr btn btn-primary'>Edit</button>
              </a>
            </div>
            <div class='action delete'>
              <a href='http://192.168.15.24/deleteProduct.php?sku=".(string)$value['sku']."'>
                <button type='button' class='btn btn-danger'>Delete</button>
              </a>
            </div>
          </div>
        </td>
      </tr>
    ";
  }  
?>
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Products</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

</head>
  <!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="index.php"><img src="images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="categories.php" class="link-menu">Categorias</a></li>
      <li><a href="products.php" class="link-menu">Produtos</a></li>
      <li><a href="report.php" class="link-menu">Send Report</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="index.php" class="link-logo"><img src="images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header --><body>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Products</h1>
      <button class="btn btn-outline-dark">
        <a style="color:#ccc;"href="addProduct.php">Add new Product</a>
      </button>
    </div>
    <?php echo $returnMessage; ?>
    <table class="table table-striped">
      <tr>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>

      <?php
          echo $productsHTML;
      ?>
    </table>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="footer-image">
	  <img src="images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
	</div>
	<div class="email-content">
	  <span>go@jumpers.com.br</span>
	</div>
</footer>
 <!-- Footer --></body>
</html>
