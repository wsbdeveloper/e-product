<?php
  session_start();
    
  require_once("model/database/schemas/postgresql/postgresql.php");
  
  $status = $_GET["status"];
  
  $returnMessage = "";

  if($status === 'success'){
    $returnMessage .= "
      <div class='alert alert-success' role='alert'>
        Alteração executada com sucesso!
      </div>
    ";

  }else if($status === 'danger'){
    $returnMessage .= "
      <div class='alert alert-danger' role='alert'>
        Alteração não foi executada com sucesso!
      </div>
    ";
  }else if(!$status){
    $retunMessage = "";
  }


  function getCategories(){
    $db = new Database();
    $connect = $db->connect();

    $getCategories = "SELECT name, code FROM category;";

    $statement = $connect->prepare($getCategories);
    
    $statement->execute();

    $res = $statement->fetchAll(PDO::FETCH_ASSOC);
    
    return $res;

  }



  $categories = getCategories();

  $categoriesHTML = "";


  foreach($categories as $key => $value){
    $categoriesHTML .= "
      <tr class='data-row'>
        <td class='data-grid-td'>
          <span class='data-grid-cell-content'>".$value['name']."</span>
        </td>

        <td class='data-grid-td'>
          <span class='data-grid-cell-content'>".$value['code']."</span>
        </td>

        <td class='data-grid-td'>
          <div class='row' style='text-align: center;display: flex;justify-content: space-evenly;'>
            <div class='action edit'>
              <a href='http://192.168.15.24/editCategory.php?id=".(string)$value['code']."'>
                <button type='button' class='mr btn btn-primary'>Edit</button>
              </a>
            </div>
            <div class='action delete'>
              <a href='http://192.168.15.24/deleteCategory.php?code=".(string)$value['code']."'>
                <button type='button' class='btn btn-danger' id='button-delete'>Delete</button>
              </a>
            </div>
          </div>
        </td>
      </tr>";
  }
?>



<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Categories</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css"  media="all" href="css/style.css" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <meta name="viewport" content="width=device-width,minimum-scale=1">
  <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
  <script async src="https://cdn.ampproject.org/v0.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
</head>
  <!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="dashboard.html"><img src="images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="categories.php" class="link-menu">Categorias</a></li>
      <li><a href="products.php" class="link-menu">Produtos</a></li>
      <li><a href="report.php" class="link-menu">Send Reports</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="index.php" class="link-logo"><img src="images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header --><body>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categories</h1>
      <button class="btn btn-outline-dark">
        <a style="color:#ccc;"href="addCategory.html">Add new Category</a>
      </button>
    </div>

    <?php echo $returnMessage; ?>
    <table class="table table-striped">
      <tr class="data-row">
        <th class="data-grid-th">Name</th>
        <th class="data-grid-th">Code</th>
        <th class="data-grid-th">Action</th>
      </tr>
      <?php echo $categoriesHTML;?>
    </table>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="footer-image">
	  <img src="images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
	</div>
	<div class="email-content">
	  <span>go@jumpers.com.br</span>
	</div>
</footer>
 <!-- Footer --></body>
</html>
