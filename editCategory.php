<?php
  session_start();
  
  require_once("model/database/schemas/postgresql/postgresql.php");

  require_once("model/category.php");
  
  $code = $_GET['code'];
  
  $status = $_GET['status'];

  $modelCategory = new Category();

  $categories = $modelCategory->readCategory();

  $categoriesHTML = "";

  if($status === 'success'){
    $returnMessage = "
      <div class='alert alert-success' role='alert'>
        Alteração executada com sucesso!
      </div>
    ";
  }else if ($status === 'danger'){
    $returnMessage = "
      <div class='alert alert-danger' role='alert'>
        Alteração não foi executada com sucesso!
      </div>
    ";
  }

  foreach($categories as $value){
    $categoriesHTML .= "
        <div class='sup'>
          <label>".$value['name']."</label> 
            <b style='display:flex;flex-direction:column;align-items:center;justify-content:center;'>to :</b>
            <input data-js='".$value['name']."' 
                   class='in-cmp' 
                   type='text' 
                   id='form-".$value['name']."' 
                   onblur='this.placeholder = '".$value['name']."'' 
                   onfocus='this.placeholder = '".$value['code']."''
                   placeholder='".$value['name']."'
                   value='".$value['name']."'                    
                   autocomplete='off'
                   name='arrayEditCategories[]'
                   maxlength='100'/>
          <input type='hidden' name='arrayEditCode[]' value='".$value['code']."' />
        </div>
    ";
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="./css/estilos.css" />
    <title>Webjump | Backend Test | Categories</title>
</head>
<body>
    <header>
        <div class="header-barber">
            <b>WEB JUMP</b>
        </div>
    </header>
    <div class="back-to-index">
      <a href="http://192.168.15.24/">	&larr;</a>
    </div>
    <div class="component-header">
            <div class="component-info">
                <div class="info">
                    <div class="title-website">
                        <h1>Edit Categories : </h1>
                    </div>
                </div>
            </div>
            <?php echo $returnMessage; ?>
        </div>
        <main>
            <div class="grid-menu">
                <div class="component-form">
                    <form method="POST" action="controller/editCategories.php">
                        <?php echo $categoriesHTML; ?>
                        <button type="submit" class="form-submit" id="buttonForm">Edit Categories</button>
                    </form>
                </div>
            </div>
        </main>
    
        <script src="./js/main.js"></script>
</body>
</html>
