<?php
  session_start();

  require_once("model/database/schemas/postgresql/postgresql.php");

  function getDashboardIndexs(){
    $db = new Database();
    $connect = $db->connect();


    #Não esquecer de pegar o n for n.
    $getDashboardIndexs = "SELECT name ,
                             sku , 
                             price, 
                             describe , 
                             quantity ,
                             code_category,
                             cdnphoto FROM product";

    $statement = $connect->prepare($getDashboardIndexs);

    $statement->execute();

    $res = $statement->fetchAll(PDO::FETCH_ASSOC);

    return $res;
  }

  $getDashboard = getDashboardIndexs();

  $dashboardHTML = "";

  foreach($getDashboard as $key => $value){
    $dashboardHTML .= "
      <li>
        <div class='product-image'>
          <img src='uploads/".$value['cdnphoto']."' layout='responsive' width='164' height='145' alt='".$value['name']."' style='margin:0 auto;'/>
        </div>
        <div class='product-info'>
          <div class='product-name'>
            <span>".$value['name']."</span>
          </div>
          <div class='product-price'>
            <span class='special-price'>".$value['quantity']."</span> 
            <span> R$ : ".$value['price']."</span>
          </div>
        </div>
      </li>
    ";
  }
?>


<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Dashboard</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
  <!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="index.html"><img src="images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="categories.php" class="link-menu">Categorias</a></li>
      <li><a href="products.php" class="link-menu">Produtos</a></li>
      <li><a href="report.php" class="link-menu">Send Reports</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="index.php" class="link-logo"><img src="images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header -->
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
    <strong>You have 4 products added on this store:</strong>
    <div class="header-list-page">
      <button class="btn btn-outline-dark">
        <a style="color:#ccc;"href="addProduct.php">Add new Product</a>
   </button>
   </div>

    </div>
    <ul class="product-list">
      <?php echo $dashboardHTML; ?>
    </ul>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="footer-image">
	  <img src="images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
	</div>
	<div class="email-content">
	  <span>go@jumpers.com.br</span>
	</div>
</footer>
 <!-- Footer --></body>
</html>
