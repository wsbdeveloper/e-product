const name = document.getElementById('form-name');
const lblname = document.getElementById('lbl-name');

const telefone = document.getElementById('form-tel');
const lbltel = document.getElementById('lbl-tel');

const email = document.getElementById('form-email');
const lblmail = document.getElementById('lbl-email');

const url = document.getElementById('form-url');
const lblurl = document.getElementById('lbl-url');

const social = document.getElementById('form-social');
const lblsocial = document.getElementById('lbl-social');


name.addEventListener('focus', event => {
    event.target.style.borderColor = '#8e7dea';
    lblname.style.display = "block";
    lblname.style.color = 'rgb(142, 125, 234)';
});

name.addEventListener('blur', event => {
    event.target.style.borderColor = '#ccc';
    lblname.style.display = "none";
});

telefone.addEventListener('focus', event => {
    event.target.style.borderColor = '#8e7dea';
    lbltel.style.display = "block";
    lbltel.style.color = 'rgb(142, 125, 234)';
});

telefone.addEventListener('blur', event => {
    event.target.style.borderColor = '#ccc';
    lbltel.style.display = "none";
});

email.addEventListener('focus', event => {
    event.target.style.borderColor = '#8e7dea';
    lblmail.style.display = "block";
    lblmail.style.color = 'rgb(142, 125, 234)';
});

email.addEventListener('blur', event => {
    event.target.style.borderColor = '#ccc';
    lblmail.style.display = "none";
});

social.addEventListener('focus', () =>{
    event.target.style.borderColor = '#8e7dea';
    lblsocial.style.display = "block";
    lblsocial.style.color = 'rgb(142, 125, 234)';
});

social.addEventListener('blur', () =>{
    event.target.style.borderColor = '#ccc';
    lblsocial.style.display = "none";
});

