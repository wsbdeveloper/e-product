const mask = {
    email(value){
        return value
        .replace(/([a-zA-Z]+@)/, '$1')
    },
    telefone(value){
        return value
        .replace(/\D/g, '')
        .replace(/(\d{2})(\d)/, '($1) $2')
        .replace(/(\d{4})(\d)/, '$1-$2')
        .replace(/(\d{4})-(\d)(\d{4})/, '$1$2-$3')
    },
    money(value){
      return value
        .replace(/\D/g,'')
        .replace(/(\d{2})(\d)(\d{3})(\d)(\d{3})(\d)(\d{2})/,'$1.$2.$3,$4')
    }
}

document.querySelectorAll('input').forEach(($input) =>{
    const field = $input.dataset.js

    $input.addEventListener('input', e =>{
        e.target.value = mask[field](e.target.value)
    }, false)
});

